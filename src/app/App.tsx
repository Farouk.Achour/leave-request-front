// import routes, { renderRoutes } from '@src/modules/shared/routes'
import { useAppSelector } from '@src/modules/shared/store'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet-async'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'

import routes, { renderRoutes } from '@src/modules/shared/routes'
import { Toaster } from 'react-hot-toast'

// import { AbilityContextProvider } from '@src/modules/shared/contexts/AbilityContext'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 0,
    },
  },
})

const App = () => {
  // get translation.json file from public/locales
  const { i18n } = useTranslation('translation')

  document.body.dir = i18n?.dir()

  const theme = useAppSelector((state) => state.theme.mode)

  return (
    <div id={theme}>
      <QueryClientProvider client={queryClient}>
        <Helmet>
          <title>Leavemetry</title>
        </Helmet>
        {/* add AbilityProvider if needed
      <AbilityContextProvider roles={user?.roles}> */}

        {renderRoutes(routes)}
        {/* </AbilityContextProvider> */}

        <Toaster
          position="top-center"
          gutter={12}
          containerStyle={{ margin: '8px' }}
          toastOptions={{
            success: {
              duration: 3000,
            },
            error: {
              duration: 5000,
            },
            style: {
              fontSize: '16px',
              maxWidth: '500px',
              padding: '16px 24px',
              backgroundColor: '#fff',
              color: '#555',
            },
          }}
        />
      </QueryClientProvider>
    </div>
  )
}

export default App
