import AuthProvider from './modules/shared/providers/AuthProvider'
import { HelmetProvider } from 'react-helmet-async'
import { BrowserRouter } from 'react-router-dom'
import { store } from './modules/shared/store'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import { Suspense } from 'react'
import App from './app/App'
import './app/index.scss'
import './i18n'
import { GoogleOAuthProvider } from '@react-oauth/google'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <HelmetProvider>
    <Provider store={store}>
      <AuthProvider>
        <BrowserRouter>
          <Suspense>
            <GoogleOAuthProvider clientId="95437574485-vb6a29jio38btsbglt7dpmjqo7vqu9me.apps.googleusercontent.com">
              <App />
            </GoogleOAuthProvider>
          </Suspense>
        </BrowserRouter>
      </AuthProvider>
    </Provider>
  </HelmetProvider>
)
