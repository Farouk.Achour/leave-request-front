import { Button, DatePicker, Form, Input, Select } from 'antd'
import { useCreateLeave } from '../hooks/useCreateLeave'
import { useUser } from '@src/modules/auth/hook/useUser'
import { useMyLeaves } from '@src/modules/space/hooks/useMyLeaves'
import { useLeaves } from '@src/modules/leaves/hooks/useLeaves'
import { useEmployees } from '@src/modules/employees/hooks/useEmployees'
import LeavesTable, { ILeave } from '@src/modules/space/components/LeavesTable/LeavesTable'

import { ReactComponent as SickIcon } from '../../shared/assets/icons/dashboard/sick.svg'
import { ReactComponent as VacationIcon } from '../../shared/assets/icons/dashboard/vacation.svg'
import { ReactComponent as PersonalIcon } from '../../shared/assets/icons/dashboard/personal.svg'
import { ReactComponent as CasualIcon } from '../../shared/assets/icons/dashboard/casual.svg'

import { ReactComponent as InOfficeIcon } from '../../shared/assets/icons/dashboard/inOffice.svg'
import { ReactComponent as InVacationIcon } from '../../shared/assets/icons/dashboard/inVaction.svg'
import { ReactComponent as TotalLeavesIcon } from '../../shared/assets/icons/dashboard/totalLeaves.svg'
import { ReactComponent as PendingLeavesIcon } from '../../shared/assets/icons/dashboard/pendingLeaves.svg'

import io, { Socket } from 'socket.io-client'
import { useEffect, useState } from 'react'
import { fDate } from '@src/modules/shared/utils/formatTime'
import { calculateLeaveBalance } from '@src/modules/shared/utils/calculateLeaveBalance'

const FeedbackLayout = () => {
  const { createLeave } = useCreateLeave()
  const { user } = useUser()
  const { employees } = useEmployees()

  const { leaves: myLeaves } = useMyLeaves(user?.id)
  const myAcceptedLeaves = myLeaves?.filter((leave: ILeave) => leave.status?.includes('approved'))

  const { leaves: allLeaves } = useLeaves()

  const pendingLeaves = allLeaves?.filter((leave: ILeave) => leave.status?.includes('pending'))
  const acceptedLeaves = allLeaves?.filter((leave: ILeave) => leave.status?.includes('approved'))
  const lastLeaves = allLeaves?.slice(allLeaves.length - 5)?.reverse()

  const employeesInVacation = acceptedLeaves?.reduce((acc: number, cur: ILeave) => {
    const from = new Date(cur.from).valueOf()
    const to = new Date(cur.to).valueOf()
    const now = Date.now()
    if (from <= now && now <= to) {
      acc += 1
    }
    return acc
  }, 0)

  const employeesAtWork = employees?.length && employees?.length - employeesInVacation

  const role = user?.role?.id

  const NumberOfSickLeaves = myLeaves?.filter((leave: ILeave) => leave.type === 'sick').length
  const NumberOfVacationLeaves = myLeaves?.filter((leave: ILeave) => leave.type === 'vacation')
    .length
  const NumberOfPersonalLeaves = myLeaves?.filter((leave: ILeave) => leave.type === 'personal')
    .length
  const NumberOfCasualLeaves = myLeaves?.filter((leave: ILeave) => leave.type === 'casual').length

  const [socket, setSocket] = useState<Socket>()

  function addLeaveRequest({ leave }: { leave: ILeave }) {
    const newLeave = {
      ...leave,
      user: user?.id,
      status: 'pending-hr',
    }
    socket?.emit('notification', {
      description: `${user.fullName} requested a leave from ${fDate(leave.from)} to ${fDate(
        leave.to
      )}`,
      viewer: 'HR',
    })
    createLeave(newLeave)
  }

  useEffect(() => {
    const newSocket = io('http://localhost:8001')
    setSocket(newSocket)
  }, [setSocket])

  // Balance Logic
  const leaveBalance = calculateLeaveBalance(user, myAcceptedLeaves) || 0

  const LeaveStats = () => (
    <div className="leave-balance-container">
      <h1 className="dashboard-headers">My Leave Balance</h1>
      <div className="leave-balance">
        <div className="leave-balance-item">
          <SickIcon />
          <span>Sick Leave</span>
          <span>{NumberOfSickLeaves} Days</span>
        </div>
        <div className="leave-balance-item">
          <VacationIcon />
          <span>Vacation Leave</span>
          <span>{NumberOfVacationLeaves} Days</span>
        </div>
        <div className="leave-balance-item">
          <PersonalIcon />
          <span>Personal Leave</span>
          <span>{NumberOfPersonalLeaves} Days</span>
        </div>
        <div className="leave-balance-item">
          <CasualIcon />
          <span>Casual Leave</span>
          <span>{NumberOfCasualLeaves} Days</span>
        </div>
      </div>
    </div>
  )

  return (
    <>
      {role === 2 && (
        <div className="em-dashboard-layout">
          <div className="left-side">
            <LeaveStats />
            <Form className="form-leave" layout="vertical" onFinish={addLeaveRequest}>
              <h1 className="dashboard-headers">Ask For a Leave</h1>

              <Form.Item
                name={['leave', 'type']}
                label="Type of leave"
                rules={[{ required: true }]}
                style={{ maxWidth: '940px' }}
              >
                <Select style={{ height: '44px' }} placeholder="Select the leave type">
                  <Select.Option value="sick">Sick</Select.Option>
                  <Select.Option value="vacation">Vacation</Select.Option>
                  <Select.Option value="casual">Casual</Select.Option>
                  <Select.Option value="personal">Personal</Select.Option>
                </Select>
              </Form.Item>

              <Form.Item
                name={['leave', 'description']}
                label="Description"
                rules={[{ required: true }]}
                style={{ maxWidth: '940px' }}
              >
                <Input style={{ height: '44px' }} />
              </Form.Item>

              <div>
                <Form.Item
                  label="Date"
                  name={['leave', 'from']}
                  rules={[{ required: true }]}
                  style={{ margin: 0 }}
                >
                  <DatePicker
                    style={{
                      height: '44px',
                      width: '100%',
                      maxWidth: '940px',
                      borderBottom: 0,
                      borderRadius: '6px 6px 0 0',
                    }}
                    placeholder="From"
                  />
                </Form.Item>
                <Form.Item name={['leave', 'to']} rules={[{ required: true }]}>
                  <DatePicker
                    style={{
                      height: '44px',
                      width: '100%',
                      maxWidth: '940px',
                      borderRadius: '0 0 6px 6px',
                    }}
                    placeholder="To"
                  />
                </Form.Item>
              </div>

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Apply For a Leave
                </Button>
              </Form.Item>
            </Form>
          </div>
          <div className="right-side">
            <div className="employee-card">
              {user?.photo?.path && (
                <img src={user?.photo?.path} alt="avatar" className="avatar-dashboard" />
              )}

              <div>
                <h2 className="fullName">{user?.fullName}</h2>

                <h3 className="user-position">{user?.job}</h3>
              </div>

              <div>
                <h3>Leave Balance</h3>
                <h3 className="leave-balance">{leaveBalance} Days Left</h3>
              </div>
            </div>
          </div>
        </div>
      )}

      {role === 1 && (
        <div className="hr-dashboard-layout">
          <div className="analytics">
            <h1 className="dashboard-headers">This month Analytics</h1>
            <div className="month-analytics">
              <div className="month-analytics-item">
                <div className="flex">
                  <span className="header">Employee in Vacation</span>
                  <InVacationIcon />
                </div>
                <span>{employeesInVacation}</span>
              </div>
              <div className="month-analytics-item">
                <div className="flex">
                  <span className="header">Employee in Office</span>
                  <InOfficeIcon />
                </div>
                <span>{employeesAtWork}</span>
              </div>
              <div className="month-analytics-item">
                <div className="flex">
                  <span className="header">Total Leave Requested</span>
                  <TotalLeavesIcon />
                </div>
                <span>{allLeaves?.length}</span>
              </div>
              <div className="month-analytics-item">
                <div className="flex">
                  <span className="header">Pending Leaves</span>
                  <PendingLeavesIcon />
                </div>
                <span>{pendingLeaves?.length}</span>
              </div>
            </div>
          </div>
          <div>
            <h1 className="dashboard-headers">Last Leaves Request</h1>
            <LeavesTable leaves={lastLeaves} />
          </div>
          <LeaveStats />
        </div>
      )}
    </>
  )
}

export default FeedbackLayout
