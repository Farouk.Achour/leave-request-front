import { useMutation } from '@tanstack/react-query'
import { createLeave as createLeaveApi } from '@src/modules/shared/services/apiLeaves'
import { toast } from 'react-hot-toast'

export function useCreateLeave() {
  const { mutate: createLeave, isSuccess } = useMutation({
    mutationFn: (args: any) => createLeaveApi(args),
    onSuccess: () => {
      toast.success('Leave request is created')
    },
    onError: (err) => {
      toast.error(err.message)
    },
  })

  return { createLeave, isSuccess }
}
