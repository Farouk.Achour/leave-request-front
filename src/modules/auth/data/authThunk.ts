/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAsyncThunk } from '@reduxjs/toolkit'
import { ForgotPasswordPayload, LoginPayload, ResetPasswordPayload } from './authTypes'
import axiosInstance from '@src/modules/shared/utils/axios'

export const login = createAsyncThunk(
  'auth/login',
  async (query: LoginPayload, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.post('auth/email/login', query)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const loginWithGoogle = createAsyncThunk(
  'auth/google/login',
  async (query: any, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.post('auth/google/login', query)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const forgotPassword = createAsyncThunk(
  'auth/forgot-password',
  async (query: ForgotPasswordPayload, { rejectWithValue }) => {
    try {
      const data = axiosInstance.post('auth/forgot/password', query)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const resetPassword = createAsyncThunk(
  'auth/reset-password',
  async (query: ResetPasswordPayload, { rejectWithValue }) => {
    try {
      const data = await axiosInstance.post('auth/reset/password', query)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const logout = createAsyncThunk('auth/logout', async (_, { rejectWithValue }) => {
  try {
    await axiosInstance.post('/auth/logout')

    return null
  } catch (err: any) {
    return rejectWithValue(err)
  }
})
