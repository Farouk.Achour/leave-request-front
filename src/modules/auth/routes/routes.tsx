/* eslint-disable @typescript-eslint/no-explicit-any */
import GuestLayout from '@src/modules/shared/layout/GuestLayout/GuestLayout'
import GuestGuard from '../../shared/guards/GuestGuard'
import { Navigate, RouteProps } from 'react-router-dom'
import { Fragment, lazy } from 'react'
import { PATH } from './paths'

type RouteConfig = {
  exact: boolean | null
  path: string
  component: React.ComponentType<any>
  guard?: React.ComponentType<any> | typeof Fragment | any
  layout?: React.ComponentType<any> | typeof Fragment
} & RouteProps

const routes: RouteConfig[] = [
  // GuestGuard Routes
  {
    exact: true,
    path: PATH.ROOT,
    guard: GuestGuard,
    component: () => <Navigate to="/login" />,
  },
  {
    exact: true,
    guard: GuestGuard,
    path: PATH.LOGIN,
    component: lazy(() => import('../features/Login/Login')),
    layout: GuestLayout,
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/auth/forgot-password',
    component: lazy(() => import('../features/ForgotPassword/ForgotPassword')),
    layout: GuestLayout,
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/auth/reset-password',
    component: lazy(() => import('../features/ResetPassword/ResetPassword')),
    layout: GuestLayout,
  },
]

export default routes
