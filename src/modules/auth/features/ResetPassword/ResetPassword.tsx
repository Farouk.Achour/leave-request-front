import Button from '@src/modules/shared/components/Button/Button'
import { useAppDispatch } from '@src/modules/shared/store'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { resetPassword } from '../../data/authThunk'
import Input from '@src/modules/shared/components/Input/Input'
import { getChangedValues } from '@src/modules/shared/utils/getChangedValuesFormik'
import { useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import LogoIcon from '../../../shared/assets/icons/sidebar/logo.png'
import { toast } from 'react-hot-toast'

function ResetPassword() {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [submitting, setSubmitting] = useState(false)

  const [searchParams] = useSearchParams()

  const hash = searchParams.get('hash')

  const initialValues = {
    password: '',
    confirmPassword: '',
    hash,
  }

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object().shape({
      password: Yup.string().required('Password is required').min(6, 'Password is too short!'),
      confirmPassword: Yup.string()
        .required('Password is required')
        .min(6, 'Password is too short!'),
    }),
    onSubmit: (values) => {
      setSubmitting(true)
      const changedValues = { ...getChangedValues(values, initialValues), hash }

      if (changedValues.password !== changedValues.confirmPassword) {
        toast.error('Password and confirmPassword should be the same!')
        setSubmitting(false)
      } else {
        dispatch(resetPassword(changedValues))
          .unwrap()
          .then(() => {
            navigate('/login')
            toast.success('Password successfully updated!')
          })
          .catch((err) => {
            toast.error(err?.message || 'something-went-wrong')
          })
          .finally(() => {
            setSubmitting(false)
          })
      }
    },
  })

  return (
    <div className="login-module">
      <img src="/src/modules/shared/assets/images/auth/auth-img.png" className="auth-img" />

      <div className="auth-container">
        <img src={LogoIcon} className="logo" alt="" />

        <form className="login-card-container" onSubmit={formik.handleSubmit}>
          <h1 className="title">Create new password</h1>

          <p className="description">
            Your new password must be different from previous used password
          </p>

          <Input
            name="password"
            formik={formik}
            variant="secondary"
            placeholder="Your password"
            label="Password"
            required={true}
          />

          <Input
            name="confirmPassword"
            formik={formik}
            variant="secondary"
            placeholder="Confirm your password"
            label="Confirm Password"
            type="password"
            required={true}
          />

          <Button label={'Reset password'} type={'submit'} loading={submitting} />
        </form>
      </div>
    </div>
  )
}

export default ResetPassword
