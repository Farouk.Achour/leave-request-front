import Button from '@src/modules/shared/components/Button/Button'
import { useAppDispatch } from '@src/modules/shared/store'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { login, loginWithGoogle } from '../../data/authThunk'
import Input from '@src/modules/shared/components/Input/Input'
import { getChangedValues } from '@src/modules/shared/utils/getChangedValuesFormik'
import { useState } from 'react'
import LogoIcon from '../../../shared/assets/icons/sidebar/logo.png'
import { toast } from 'react-hot-toast'
import { Link } from 'react-router-dom'
import { GoogleLogin } from '@react-oauth/google'

const initialValues = {
  username: '',
  password: '',
}

const Login = () => {
  const dispatch = useAppDispatch()

  const [submitting, setSubmitting] = useState(false)

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object().shape({
      email: Yup.string().required('Email is required'),
      password: Yup.string().required('Password is required').min(6, 'Password is too short!'),
    }),
    onSubmit: (values) => {
      setSubmitting(true)
      const changedValues = getChangedValues(values, initialValues)

      dispatch(login(changedValues))
        .unwrap()
        .then(() => {})
        .catch((err) => {
          toast.error(err?.message || 'something-went-wrong')
        })
        .finally(() => {
          setSubmitting(false)
        })
    },
  })

  const responseMessage = async (response: any) => {
    dispatch(loginWithGoogle({ idToken: response?.credential }))
  }
  const errorMessage = () => {
    throw new Error('error')
  }

  return (
    <div className="login-module">
      <img src="src/modules/shared/assets/images/auth/auth-img.png" className="auth-img" />

      <div className="auth-container">
        <img src={LogoIcon} className="logo" alt="" />

        <form className="login-card-container" onSubmit={formik.handleSubmit}>
          <h1 className="title">Sign In to your Account</h1>

          <p className="description">Fill up your account Details</p>

          <Input
            name="email"
            formik={formik}
            variant="secondary"
            placeholder="Enter your email"
            label="Email Address"
            required={true}
          />

          <Input
            name="password"
            formik={formik}
            variant="secondary"
            placeholder="Enter your password"
            label="Password"
            type="password"
            required={true}
          />

          <Button label={'Login'} type={'submit'} loading={submitting} />

          <GoogleLogin onSuccess={responseMessage} onError={errorMessage} width={378} />

          <Link to={'/auth/forgot-password'} className="link">
            Forgot Password?
          </Link>
        </form>
      </div>
    </div>
  )
}

export default Login
