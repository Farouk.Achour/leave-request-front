import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import { useNavigate } from 'react-router-dom'
import { useUser } from '@src/modules/auth/hook/useUser'
import { useHolidays } from '../../hooks/useHolidays'
import { formatDate } from '@fullcalendar/core/index.js'
import { IHoliday } from '../../components/HolidayRow/HolidayRow'

function Holidays() {
  const navigate = useNavigate()

  const { user } = useUser()

  const { holidays, isLoading } = useHolidays()

  if (isLoading) return null

  const role = user?.role.id

  function handleAddHolidays() {
    navigate('/settings/holidays/add')
  }

  const events = holidays?.map((holiday: IHoliday) => {
    const startDate = new Date(holiday?.date)
    let start: any = formatDate(startDate).split('/')
    const year1 = start[2]
    const month1 = start[0].length === 2 ? start[0] : '0' + start[0]
    const day1 = start[1].length === 2 ? start[1] : '0' + start[1]

    start = year1 + '-' + month1 + '-' + day1

    const endDate = new Date(start)
    endDate.setDate(endDate.getDate() + holiday?.duration)
    let end: any = formatDate(endDate).split('/')
    const year2 = end[2]
    const month2 = end[0].length === 2 ? end[0] : '0' + end[0]
    const day2 = end[1].length === 2 ? end[1] : '0' + end[1]

    end = year2 + '-' + month2 + '-' + day2

    const newHoliday = {
      start,
      end,
      color: holiday?.type === 'paid' ? 'green' : 'red',
      display: 'background',
      title: holiday?.name,
    }
    return newHoliday
  })

  return (
    <>
      <FullCalendar
        plugins={[dayGridPlugin]}
        initialView="dayGridMonth"
        height={'80vh'}
        events={events || {}}
        customButtons={{
          myCustomButton: {
            text: 'Add a New Holiday',
            click: handleAddHolidays,
          },
        }}
        headerToolbar={{
          start: 'title',
          center: `${role === 1 ? 'myCustomButton' : ''}`,
        }}
      />
      <div className="calendar-footer">
        <div className="calendar-footer-filter">
          <div className="green-box"></div>
          <div>Non-working and paid public holidays.</div>
        </div>
        <div className="calendar-footer-filter">
          <div className="red-box"></div>
          <div>Non-working and unpaid public holidays.</div>
        </div>
        <div className="calendar-footer-filter">
          <div className="yellow-box"></div>
          <div>Today</div>
        </div>
      </div>
    </>
  )
}

export default Holidays
