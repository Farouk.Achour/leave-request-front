import DepartmentForm from '../../components/DepartmentForm/DepartmentForm'
import { useCreateDepartment } from '../../hooks/useCreateDepartment'

function AddDepartment() {
  const { createDepartment } = useCreateDepartment()

  return <DepartmentForm action="add" createDepartment={createDepartment} />
}

export default AddDepartment
