import { useUser } from '@src/modules/auth/hook/useUser'
import EmployeeForm from '@src/modules/employees/components/EmployeeForm/EmployeeForm'
import { useEditEmployee } from '@src/modules/employees/hooks/useEditEmployee'

function UserSettings() {
  const { editEmployee } = useEditEmployee()

  const { user } = useUser()

  return <EmployeeForm action="edit" userMode={true} editEmployee={editEmployee} employee={user} />
}

export default UserSettings
