import Button from '@src/modules/shared/components/Button/Button'
import { useNavigate } from 'react-router-dom'
import { useDepartments } from '../../hooks/useDepartments'
import DepartmentCard, { IDepartment } from '../../components/DepartmentCard/DepartmentCard'

function Department() {
  const navigate = useNavigate()
  const { departments, isLoading } = useDepartments()
  if (isLoading) return null

  function goToAddDepartmentRoute() {
    navigate('/settings/department/add')
  }

  return (
    <>
      <div className="department-header">
        <h1>Departments</h1>
        <Button onClick={goToAddDepartmentRoute}>＋ Add New Department</Button>
      </div>
      <div className="departments">
        {departments?.map((department: IDepartment) => (
          <DepartmentCard department={department} key={department?.id} />
        ))}
      </div>
    </>
  )
}

export default Department
