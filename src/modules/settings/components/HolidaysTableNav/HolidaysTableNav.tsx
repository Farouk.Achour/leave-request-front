function HolidaysTableNav() {
  return (
    <>
      <div className="holidays-table-nav-el holidays-table-el">Name</div>
      <div className="holidays-table-nav-el holidays-table-el">Date</div>
      <div className="holidays-table-nav-el holidays-table-el">Duration</div>
      <div className="holidays-table-nav-el holidays-table-el">Type</div>
      <div className="holidays-table-nav-el holidays-table-el"></div>
    </>
  )
}

export default HolidaysTableNav
