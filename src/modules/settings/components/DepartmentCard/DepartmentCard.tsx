import { ReactComponent as EditIcon } from '../../../shared/assets/icons/edit.svg'
import { ReactComponent as TrashIcon } from '../../../shared/assets/icons/trash.svg'
import { useDeleteDepartment } from '../../hooks/useDeleteDepartment'
import useNavigateParams from '@src/modules/shared/hooks/useNavigateParams'

export interface IDepartment {
  id: number
  created_at: string
  photo: any
  name: string
  chief: string
  description: string
}

function DepartmentCard({ department }: { department: IDepartment }) {
  const navigateParams = useNavigateParams()

  const { deleteDepartment } = useDeleteDepartment()

  const { photo, name, description, chief } = department

  function handleClickOnEdit() {
    navigateParams('/settings/department/edit', {
      id: department?.id + '',
    })
  }

  function handleDeleteDepartment() {
    deleteDepartment(department?.id)
  }

  return (
    <div className="department-card">
      <img src={photo?.path} alt="department-photo" className="department-img" />
      <h1>{name}</h1>
      <p>{description}</p>
      <h3>{chief}</h3>

      <div className="department-detail-toolbar">
        <TrashIcon className="department-detail-icon" onClick={handleDeleteDepartment} />
        <EditIcon className="department-detail-icon" onClick={handleClickOnEdit} />
      </div>
    </div>
  )
}

export default DepartmentCard
