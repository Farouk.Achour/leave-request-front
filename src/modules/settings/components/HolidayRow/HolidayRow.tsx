import { useState } from 'react'
import { ReactComponent as EditIcon } from '../../../shared/assets/icons/edit.svg'
import { ReactComponent as TrashIcon } from '../../../shared/assets/icons/trash.svg'
import AddHolidayForm from '../../features/AddHolidayForm/AddHolidayForm'
import { useEditHoliday } from '../../hooks/useEditHoliday'
import { useDeleteHoliday } from '../../hooks/useDeleteHoliday'

export interface IHoliday {
  id: number
  created_at: string
  name: string
  date: string
  duration: number
  type: string
}

function HolidayRow({ holiday }: { holiday: IHoliday }) {
  const [editMode, setEditMode] = useState(false)
  const { id, name, date, duration, type } = holiday
  const { editHoliday } = useEditHoliday()
  const { deleteHoliday } = useDeleteHoliday()

  const newDate = new Date(new Date(date).setDate(new Date(date).getDate() + 1)).toISOString()

  function handleEditMode() {
    setEditMode(true)
  }

  function handleDeleteHoliday() {
    deleteHoliday(id)
  }

  let typeEl

  if (type === 'paid') {
    typeEl = (
      <div className="paid">
        <div className="paid">Paid</div>
      </div>
    )
  }

  if (type === 'unpaid') {
    typeEl = (
      <div className="unpaid">
        <div className="unpaid">Unpaid</div>
      </div>
    )
  }

  return (
    <>
      {editMode ? (
        <AddHolidayForm
          action="edit"
          holiday={holiday}
          setEditMode={setEditMode}
          editHoliday={editHoliday}
        />
      ) : (
        <>
          <div className="holidays-table-el">{name}</div>
          <div className="holidays-table-el">{newDate.toString().split('T')[0]}</div>
          <div className="holidays-table-el">
            {duration} {duration === 1 ? 'Day' : 'Days'}
          </div>
          <div className="holidays-table-el">{typeEl}</div>
          <div className="holidays-table-el icons-toolbar">
            <EditIcon className="icons-toolbar-item" onClick={handleEditMode} />{' '}
            <TrashIcon className="icons-toolbar-item" onClick={handleDeleteHoliday} />
          </div>
        </>
      )}
    </>
  )
}

export default HolidayRow
