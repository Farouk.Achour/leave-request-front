import { useMutation } from '@tanstack/react-query'
import { useNavigate } from 'react-router-dom'
import { createDepartment as createDepartmentApi } from '@src/modules/shared/services/apiDepartments'
import { toast } from 'react-hot-toast'

export function useCreateDepartment() {
  const navigate = useNavigate()

  const { mutate: createDepartment, isSuccess } = useMutation({
    mutationFn: (args: any) => createDepartmentApi(args),
    onSuccess: () => {
      toast.success('Department is created')
      navigate('/settings/department')
    },
    onError: (err) => {
      toast.error(err.message)
    },
  })

  return { createDepartment, isSuccess }
}
