import { useMutation, useQueryClient } from '@tanstack/react-query'
import { deleteHoliday as deleteHolidayApi } from '../../shared/services/apiHolidays'
import { toast } from 'react-hot-toast'

export function useDeleteHoliday() {
  const queryClient = useQueryClient()

  const { mutate: deleteHoliday } = useMutation({
    mutationFn: deleteHolidayApi,
    onSuccess: () => {
      toast.success('Holiday successfully deleted')

      queryClient.invalidateQueries({
        queryKey: ['holidays'],
      })
    },
    onError: (err) => toast.error(err.message),
  })

  return { deleteHoliday }
}
