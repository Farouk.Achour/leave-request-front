import { getHolidays } from '@src/modules/shared/services/apiHolidays'
import { useQuery } from '@tanstack/react-query'

export function useHolidays() {
  const { isLoading, data: holidays } = useQuery({
    queryKey: ['holidays'],
    queryFn: getHolidays,
  })

  return { isLoading, holidays }
}
