import { useMutation, useQueryClient } from '@tanstack/react-query'
import { createHoliday as createHolidayApi } from '@src/modules/shared/services/apiHolidays'
import { toast } from 'react-hot-toast'

export function useCreateHoliday() {
  const queryClient = useQueryClient()

  const { mutate: createHoliday, isSuccess } = useMutation({
    mutationFn: (args: any) => createHolidayApi(args),
    onSuccess: () => {
      toast.success('Holiday is created')

      queryClient.invalidateQueries({
        queryKey: ['holidays'],
      })
    },
    onError: (err) => toast.error(err.message),
  })

  return { createHoliday, isSuccess }
}
