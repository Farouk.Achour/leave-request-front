import { updateDepartment } from '@src/modules/shared/services/apiDepartments'
import { useMutation } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'
import { useNavigate } from 'react-router-dom'

export function useEditDepartment() {
  const navigate = useNavigate()

  const { mutate: editDepartment } = useMutation({
    mutationFn: ({ id, newDepartmentData }: any) => updateDepartment(id, newDepartmentData),
    onSuccess: () => {
      toast.success('Department successfully edited')
      navigate('/settings/department')
    },
    onError: (err) => toast.error(err.message),
  })

  return { editDepartment }
}
