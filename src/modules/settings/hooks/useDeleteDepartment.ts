import { useMutation, useQueryClient } from '@tanstack/react-query'
import { deleteDepartment as deleteDepartmentApi } from '../../shared/services/apiDepartments'
import { toast } from 'react-hot-toast'

export function useDeleteDepartment() {
  const queryClient = useQueryClient()

  const { mutate: deleteDepartment } = useMutation({
    mutationFn: deleteDepartmentApi,

    onSuccess: () => {
      toast.success('Department successfully deleted')

      queryClient.invalidateQueries({
        queryKey: ['departments'],
      })
    },
    onError: (err) => toast.error(err.message),
  })

  return { deleteDepartment }
}
