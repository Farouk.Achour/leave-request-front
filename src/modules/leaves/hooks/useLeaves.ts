import { getAllLeaves } from '@src/modules/shared/services/apiLeaves'
import { useQuery } from '@tanstack/react-query'

export function useLeaves() {
  const { isLoading, data: leaves } = useQuery({
    queryKey: ['leaves'],
    queryFn: getAllLeaves,
  })

  return { leaves, isLoading }
}
