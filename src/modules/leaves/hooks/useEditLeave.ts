import { updateLeave } from '@src/modules/shared/services/apiLeaves'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'

export function useEditLeave() {
  const queryClient = useQueryClient()

  const { mutate: editLeave } = useMutation({
    mutationFn: ({ id, newLeaveData }: any) => {
      return updateLeave(id, newLeaveData)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['leaves'],
      })
    },
    onError: (err) => toast.error(err.message),
  })

  return { editLeave }
}
