import { useEmployeeById } from '@src/modules/employees/hooks/useEmployeeById'
import { fDate } from '@src/modules/shared/utils/formatTime'
import { useEditLeave } from '@src/modules/leaves/hooks/useEditLeave'
import { ReactComponent as RejectIcon } from '../../../shared/assets/icons/reject.svg'
import { ReactComponent as NegotiateIcon } from '../../../shared/assets/icons/time.svg'
import { ReactComponent as AcceptIcon } from '../../../shared/assets/icons/tick.svg'
import { ReactComponent as RingIcon } from '../../../shared/assets/icons/leaves/ring.svg'
import { ReactComponent as SickIcon } from '../../../shared/assets/icons/dashboard/sick.svg'
import { ReactComponent as VacationIcon } from '../../../shared/assets/icons/dashboard/vacation.svg'
import { ReactComponent as PersonalIcon } from '../../../shared/assets/icons/dashboard/personal.svg'
import { ReactComponent as CasualIcon } from '../../../shared/assets/icons/dashboard/casual.svg'
import { useEffect, useState } from 'react'
import io, { Socket } from 'socket.io-client'

function LeaveRow({ leave, hrControl }: any) {
  const { employee, isLoading } = useEmployeeById(leave?.user?.id)
  const { editLeave } = useEditLeave()

  const [socket, setSocket] = useState<Socket>()

  useEffect(() => {
    const newSocket = io('http://localhost:8001')
    setSocket(newSocket)
  }, [setSocket])

  if (isLoading) return null
  const { photo, fullName, job } = employee
  const { id, from, to, type, status } = leave

  function formatDateToDayMonth(date: string) {
    return fDate(date)?.split(' ').slice(0, 2).join(' ')
  }

  let leaveStatus
  if (status.includes('tech'))
    leaveStatus = (
      <div className="leave-status">
        {' '}
        <RingIcon className="yellow icon" />{' '}
        <span className="yellow text">Waiting for Technical approval</span>
      </div>
    )
  if (status.includes('hr'))
    leaveStatus = (
      <div className="leave-status">
        <RingIcon className="yellow icon" />{' '}
        <span className="yellow text">Waiting for HR approval</span>
      </div>
    )
  if (status.includes('approved'))
    leaveStatus = (
      <div className="leave-status">
        <RingIcon className="green icon" /> <span className="green text">Approved</span>
      </div>
    )
  if (status.includes('rejected'))
    leaveStatus = (
      <div className="leave-status">
        <RingIcon className="red icon" /> <span className="red text">Rejected</span>
      </div>
    )
  if (status.includes('nego'))
    leaveStatus = (
      <div className="leave-status">
        <RingIcon className="orange icon" /> <span className="orange text">Negotiation</span>
      </div>
    )

  let leaveType
  if (type.at(0) === 'v')
    leaveType = (
      <div className="leave-status">
        <VacationIcon />
        <span className="orange text">
          {type.at(0).toUpperCase()}
          {type.slice(1)} Leave
        </span>
      </div>
    )

  if (type.at(0) === 'p')
    leaveType = (
      <div className="leave-status">
        <PersonalIcon />
        <span className="blue text">
          {type.at(0).toUpperCase()}
          {type.slice(1)} Leave
        </span>
      </div>
    )
  if (type.at(0) === 'c')
    leaveType = (
      <div className="leave-status">
        <CasualIcon />
        <span className="purple text">
          {type.at(0).toUpperCase()}
          {type.slice(1)} Leave
        </span>
      </div>
    )
  if (type.at(0) === 's')
    leaveType = (
      <div className="leave-status">
        <SickIcon />
        <span className="red text">
          {type.at(0).toUpperCase()}
          {type.slice(1)} Leave
        </span>
      </div>
    )

  function handleReject() {
    const leaveRejected = { ...leave, status: 'rejected' }
    editLeave({ id, newLeaveData: leaveRejected })

    socket?.emit('notification', {
      description: `The HR rejected the leave from ${fDate(leave.from)} to ${fDate(leave.to)}`,
      viewer: employee?.id,
    })
  }

  function handleNegotiate() {
    const leaveNegotiate = { ...leave, status: 'negotiation' }
    editLeave({ id, newLeaveData: leaveNegotiate })

    socket?.emit('notification', {
      description: `The HR want to negotiate the leave from ${fDate(leave.from)} to ${fDate(
        leave.to
      )}`,
      viewer: employee?.id,
    })
  }

  function handleAccept() {
    const leaveApproved = { ...leave, status: 'approved' }
    editLeave({ id, newLeaveData: leaveApproved })

    socket?.emit('notification', {
      description: `The HR accepted the leave from ${fDate(leave.from)} to ${fDate(leave.to)}`,
      viewer: employee?.id,
    })
  }

  return (
    <>
      <div className="leaves-table-el">
        {photo ? (
          <img className="avatar" src={photo.path} />
        ) : (
          <div className="avatar avatar-letter">{fullName.slice(0, 1)}</div>
        )}
        <div className="flex-col">
          <span className="employee-fullname">{fullName}</span>
          <span className="employee-job">{job}</span>
        </div>
      </div>
      <div className="leaves-table-el">
        {formatDateToDayMonth(from)} - {formatDateToDayMonth(to)}
      </div>
      <div className="leaves-table-el">{leaveType}</div>
      <div className="leaves-table-el">{leaveStatus}</div>
      <div className="leaves-table-el">
        {hrControl && (
          <>
            <div className="control-icon-wrapper" onClick={handleReject}>
              <RejectIcon className="control-icon" />
            </div>
            <div className="control-icon-wrapper" onClick={handleNegotiate}>
              <NegotiateIcon className="control-icon" />
            </div>
            <div className="control-icon-wrapper" onClick={handleAccept}>
              <AcceptIcon className="control-icon" />
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default LeaveRow
