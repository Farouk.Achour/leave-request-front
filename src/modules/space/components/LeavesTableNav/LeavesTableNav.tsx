interface ILeavesTableNavProps {
  hrControl: boolean | undefined
}

function LeavesTableNav({ hrControl }: ILeavesTableNavProps) {
  return (
    <>
      <div className="leaves-table-nav-el leaves-table-el">Name</div>
      <div className="leaves-table-nav-el leaves-table-el">From - To</div>
      <div className="leaves-table-nav-el leaves-table-el">Type</div>
      <div className="leaves-table-nav-el leaves-table-el">Status</div>
      <div className="leaves-table-nav-el leaves-table-el">{hrControl && <>Control</>}</div>
    </>
  )
}

export default LeavesTableNav
