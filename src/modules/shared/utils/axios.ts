import { clearTokens, getTokens, setTokens } from '../../auth/utils/token'
import axios from 'axios'

const baseURL = 'http://localhost:3000/api/v1'
const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
}

const axiosInstance = axios.create({
  baseURL,
  headers,
})

axiosInstance.interceptors.request.use(
  (config) => {
    const { access_token } = getTokens()
    if (access_token) {
      config.headers['Authorization'] = `Bearer ${access_token}`
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const previousRequest = error?.config
    if (error?.response?.status === 401 && !previousRequest?.sent) {
      previousRequest.sent = true
      try {
        const { refresh_token } = getTokens()
        const response = await axios.get(baseURL + '/auth/refresh', {
          headers: {
            // 'content-type': 'text/json',
            Authorization: `Bearer ${refresh_token}`,
          },
        })

        const { token } = response.data
        setTokens(token)
        previousRequest.headers['Authorization'] = `Bearer ${token}`
        return axiosInstance(previousRequest)
      } catch (err) {
        clearTokens()
      }
    }
    return Promise.reject((error.response && error.response.data) || 'Something went wrong!')
  }
)

export default axiosInstance
