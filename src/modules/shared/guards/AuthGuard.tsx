import { Navigate } from 'react-router-dom'
import { useAppSelector } from '../store'

interface MainLayoutProps {
  children: React.ReactNode
}

const AuthGuard = ({ children }: MainLayoutProps) => {
  const { user } = useAppSelector((state) => state?.auth)

  const role = user?.role?.id

  const path = window.location.pathname

  const isAuthenticated = useAppSelector((state) => state.auth.isAuthenticated)

  if (
    role === 2 &&
    (path.includes('employees') || path.includes('leaves') || path.includes('department'))
  )
    return <Navigate to="/dashboard" />

  return isAuthenticated ? children : <Navigate to="/login" />
}

export default AuthGuard
