import sharedRoutes from './sharedRoutes'
import authRoutes from '../../auth/routes/routes'
import dashboardRoutes from '../../dashboard/routes/routes'
import spaceRoutes from '../../space/routes/routes'
import employeesRoutes from '../../employees/routes/routes'
import leavesRoutes from '../../leaves/routes/routes'
import settingsRoutes from '../../settings/routes/routes'

const routes = [
  ...sharedRoutes,
  ...authRoutes,
  ...dashboardRoutes,
  ...spaceRoutes,
  ...employeesRoutes,
  ...leavesRoutes,
  ...settingsRoutes,
]

export default routes
