import axiosInstance from '../utils/axios'

export async function getDepartments() {
  let { data: departments } = await axiosInstance.get('departments')

  return departments
}

export async function getOneDepartmentById(id: string) {
  let { data: department } = await axiosInstance.get(`departments/${id}`)

  return department
}

export async function createDepartment(department: any) {
  const data = await axiosInstance.post('departments', department)

  return data
}

export async function updateDepartment(id: number, obj: any) {
  const data = await axiosInstance.patch(`departments/${id}`, obj)

  return data
}

export async function deleteDepartment(id: number) {
  const data = await axiosInstance.delete(`departments/${id}`)

  return data
}
