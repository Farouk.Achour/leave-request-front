import axiosInstance from '../utils/axios'

export async function getHolidays() {
  let { data: holidays } = await axiosInstance.get('holidays')

  return holidays
}

export async function createHoliday(holiday: any) {
  const data = await axiosInstance.post('holidays', holiday)

  return data
}

export async function updateHoliday(id: number, obj: any) {
  const data = await axiosInstance.patch(`holidays/${id}`, obj)

  return data
}

export async function deleteHoliday(id: number) {
  const data = await axiosInstance.delete(`holidays/${id}`)

  return data
}
