import axiosInstance from '../utils/axios'

export async function getCurrentUser() {
  const { data } = await axiosInstance.get('auth/me')

  return data
}
