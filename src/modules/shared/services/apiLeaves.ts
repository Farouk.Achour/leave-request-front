import axiosInstance from '../utils/axios'

export async function getAllLeaves() {
  const { data } = await axiosInstance.get('leaves')

  return data
}

export async function getMyLeaves(id: string) {
  const { data } = await axiosInstance.get(`leaves/${id}`)

  return data
}

export async function createLeave(leave: any) {
  const data = await axiosInstance.post('leaves', leave)

  return data
}

export async function updateLeave(id: number, obj: any) {
  const data = await axiosInstance.patch(`leaves/${id}`, obj)

  return data
}
