import axiosInstance from '../utils/axios'

export async function getEmployees() {
  const { data } = await axiosInstance.get('users')

  return data.data
}

export async function getOneEmployeeById(id: string) {
  const { data } = await axiosInstance.get(`users/${id}`)

  return data
}

export async function createEmployee(employee: any) {
  const data = await axiosInstance.post('users', employee)

  return data
}

export async function updateEmployee(id: number, obj: any) {
  const data = await axiosInstance.patch(`users/${id}`, obj)

  return data
}

export async function deleteEmployee(id: number) {
  const data = await axiosInstance.delete(`users/${id}`)

  return data
}
