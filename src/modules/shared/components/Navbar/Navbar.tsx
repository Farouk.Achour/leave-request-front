import { useLocation, useNavigate } from 'react-router-dom'
import { ReactComponent as ProfileIcon } from '../../assets/icons/sidebar/profile.svg'
import { ReactComponent as LogoutIcon } from '../../assets/icons/navbar/logout.svg'
import { ReactComponent as BellIcon } from '../../assets/icons/navbar/bell.svg'
import { useAppDispatch } from '../../store'
import { logout } from '@src/modules/auth/data/authThunk'
import { useEffect, useState } from 'react'
import Dropdown from '../DropDown/DropDown'
import CustomAvatar from '../Avatar/Avatar'
import { useUser } from '@src/modules/auth/hook/useUser'
import axiosInstance from '../../utils/axios'

const Navbar: React.FC = () => {
  const { pathname } = useLocation()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [isSettingOpen, setIsSettingOpen] = useState(false)
  const [isNotifOpen, setIsNotifOpen] = useState(false)
  const [notifications, setNotifications] = useState([])

  const handleLogout = () => {
    dispatch(logout())
  }

  const { user, isLoading } = useUser()

  const userId = user?.id

  const accountInfoItems = [
    {
      key: '1',
      label: (
        <div className="user-info-container">
          <CustomAvatar image={user?.photo?.path} text={user?.email || 'User'} size={40} />

          <div className="navbar-account-info">
            <p className="sidebar-accountinfo-item">{user?.email}</p>
            <p>{user?.role?.id === 1 ? 'HR' : 'Employee'}</p>
          </div>
        </div>
      ),
      disabled: true,
      onClick: () => navigate('/settings/user'),
    },
    {
      key: '2',
      label: <p>Profile</p>,
      icon: <ProfileIcon style={{ stroke: 'black', width: '18px', height: '18px' }} />,
      onClick: () => navigate('/settings/user'),
    },
    {
      key: '4',
      label: <p>logout</p>,
      icon: <LogoutIcon style={{ stroke: 'black', width: '18px', height: '18px' }} />,
      onClick: handleLogout,
    },
  ]

  useEffect(
    function () {
      async function getNotifs() {
        const { data: notifs } =
          user?.role?.id === 1
            ? await axiosInstance('notifications')
            : await axiosInstance(`notifications/${userId}`)
        setNotifications(notifs)
      }
      getNotifs()
    },
    [user, isLoading]
  )

  const notifItems: any = notifications.map((notif: any) => {
    return { key: notif?.id, label: <p>{notif?.description}</p> }
  })

  const pathDetails = pathname.split('/')[2]
  let navPathDetail = ''
  if (pathDetails === 'add') navPathDetail = 'Add New Employee'
  if (pathDetails === 'edit') navPathDetail = 'Edit Employee'
  if (pathDetails === 'department') navPathDetail = 'Departments'
  if (pathDetails === 'user') navPathDetail = 'User'
  if (pathDetails === 'holidays') navPathDetail = 'Holidays'
  if (pathDetails === 'info') navPathDetail = 'Details'

  return (
    <div className="navbar">
      <div className="navbar-left">
        <p className="navbar-left-title">
          {pathname.split('/')[2] ? (
            <span className="grey-path">{pathname.split('/')[1]} / </span>
          ) : (
            pathname.split('/')[1]
          )}
          {navPathDetail}
        </p>
      </div>
      <div className="navbar-right">
        <Dropdown
          isOpen={isSettingOpen}
          setIsOpen={setIsSettingOpen}
          items={accountInfoItems}
          triggerElement={
            <button onClick={() => setIsSettingOpen(true)} className="navbar-avatar-btn">
              <CustomAvatar image={user?.photo?.path} text="User" size={40} />
            </button>
          }
        ></Dropdown>
        <Dropdown
          isOpen={isNotifOpen}
          setIsOpen={setIsNotifOpen}
          items={notifItems}
          triggerElement={
            <button onClick={() => setIsNotifOpen(true)} className="navbar-avatar-btn">
              <BellIcon className="notif-icon" />
            </button>
          }
          large={true}
        ></Dropdown>
      </div>
    </div>
  )
}

export default Navbar
