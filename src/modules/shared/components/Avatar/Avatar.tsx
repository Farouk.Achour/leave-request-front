import { useUser } from '@src/modules/auth/hook/useUser'
import React from 'react'
export interface AvatarProps {
  image?: string | null
  text: string
  size?: number
}

const CustomAvatar: React.FC<AvatarProps> = ({ image, text, size = 30 }) => {
  const avatarStyle = {
    width: `${size}px`,
    height: `${size}px`,
    lineHeight: `${size}px`,
  }

  const { user } = useUser()

  return (
    <div className="custom-avatar" style={avatarStyle}>
      {image ? (
        <img src={image} alt={text} />
      ) : (
        <span style={{ fontSize: `${size / 2.5}px` }}>{user?.email?.charAt(0).toUpperCase()}</span>
      )}
    </div>
  )
}

export default CustomAvatar
