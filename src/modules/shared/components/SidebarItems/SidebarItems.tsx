import { SIDEBARITEMS } from '../Sidebar/items'
import React, { useRef, useState } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useUser } from '@src/modules/auth/hook/useUser'
import { ReactComponent as ArrowIcon } from '../../../shared/assets/icons/arrow.svg'
import { ReactComponent as ArrowIconUp } from '../../../shared/assets/icons/arrowUp.svg'

interface ISidebarItem {
  label: string
  link: string
  icon?: React.ReactNode
  children?: ISidebarItem[]
  disabled?: boolean
}

interface ISidebarItemsProps {
  collapseSidebar: boolean
}

interface IChildItem {
  label: string
  link: string
  icon?: React.ReactNode
}

const SidebarItems: React.FC<ISidebarItemsProps> = ({ collapseSidebar }) => {
  const { pathname } = useLocation()
  const [openItem, setOpenItem] = useState<string | null>(null)
  const navigate = useNavigate()

  const determineOpenItem = (pathname: string): string | null => {
    let foundOpenItem: string | null = null
    SIDEBARITEMS.forEach((route: ISidebarItem) => {
      if (
        route.link === pathname ||
        (route.children && route.children.some((child: IChildItem) => child.link === pathname))
      ) {
        foundOpenItem = route.link
      }
    })
    return foundOpenItem
  }

  useState(() => {
    setOpenItem(determineOpenItem(pathname))
  })

  const toggleItem = (itemLink: string, hasChildren: boolean): void => {
    if (hasChildren) {
      setOpenItem(openItem === itemLink ? null : itemLink)
    } else {
      navigate(itemLink)
    }
  }

  const childContainerRef = useRef<HTMLDivElement>(null)

  const isActive = (pathname: string, routeLink: string): boolean => {
    const pathnameFirstSegment = pathname.split('/')[1]
    const routeLinkFirstSegment = routeLink.split('/')[1]
    return pathnameFirstSegment === routeLinkFirstSegment
  }

  const { user } = useUser()

  let role = user?.role?.id

  let filteredSidebar

  if (role === undefined) {
    navigate('/login')
    return null
  }
  if (role !== 1) {
    filteredSidebar = SIDEBARITEMS?.filter((route) => {
      if (route.children)
        route.children = route.children.filter((route) => route.label !== 'Department')
      return route.label !== 'Employees' && route.label !== 'Leaves'
    })
  } else filteredSidebar = SIDEBARITEMS

  return (
    <div className="sidebar-items">
      {filteredSidebar.map((route: ISidebarItem, index: number) => (
        <div key={index}>
          <div onClick={() => !route.disabled && toggleItem(route.link, !!route.children)}>
            <div
              className={`item ${isActive(pathname, route.link) ? 'active' : ''} ${
                route.children ? 'has-children' : ''
              }${route.disabled ? 'disabled-route' : ''}`}
            >
              <div
                className={`link-icon-stroke-color ${
                  pathname === route.link ? 'link-icon-stroke-color-active' : ''
                }`}
              >
                {route.icon}
              </div>
              {!collapseSidebar ? route.label : null}
              {route.children ? (
                openItem === route.link ? (
                  <ArrowIcon style={{ marginLeft: 'auto' }} />
                ) : (
                  <ArrowIconUp style={{ marginLeft: 'auto' }} />
                )
              ) : (
                ''
              )}
            </div>
          </div>
          {route.children && openItem === route.link && (
            <div className="link-child-container" ref={childContainerRef}>
              <div>
                {route.children.map((child: IChildItem, childIndex: number) => (
                  <div key={childIndex}>
                    {!collapseSidebar && (
                      <Link
                        to={child.link}
                        className={`child-item ${pathname === child.link ? 'active-child' : ''}`}
                      >
                        {child.label}
                      </Link>
                    )}
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
      ))}
    </div>
  )
}

export default SidebarItems
