import { ReactComponent as DashboardIcon } from '../../assets/icons/sidebar/dashboard.svg'
import { ReactComponent as SpaceIcon } from '../../assets/icons/sidebar/space.svg'
import { ReactComponent as EmployeesIcon } from '../../assets/icons/sidebar/employees.svg'
import { ReactComponent as LeavesIcon } from '../../assets/icons/sidebar/leaves.svg'
import { ReactComponent as SettingsIcon } from '../../assets/icons/sidebar/settings.svg'
import { Link } from 'react-router-dom'
import { UsergroupAddOutlined } from '@ant-design/icons'

export const SIDEBARITEMS = [
  {
    link: '/dashboard',
    label: 'Dashboard',
    icon: <DashboardIcon />,
  },
  {
    link: '/space',
    label: 'Space',
    icon: <SpaceIcon />,
  },
  {
    link: '/employees',
    label: 'Employees',
    icon: <EmployeesIcon />,
  },
  {
    link: '/leaves',
    label: 'Leaves',
    icon: <LeavesIcon />,
  },
  {
    link: '/settings/department',
    label: 'Settings',
    icon: <SettingsIcon />,
    children: [
      {
        link: '/settings/department',
        label: 'Department',
        icon: (
          <Link to={'/settings/department'}>
            <UsergroupAddOutlined />{' '}
          </Link>
        ),
      },
      {
        link: '/settings/user',
        label: 'User Settings',
        icon: (
          <Link to={'/settings/user'}>
            <UsergroupAddOutlined />{' '}
          </Link>
        ),
      },
      {
        link: '/settings/holidays',
        label: 'Holidays',
        icon: (
          <Link to={'/settings/holidays'}>
            <UsergroupAddOutlined />{' '}
          </Link>
        ),
      },
    ],
  },
]
