import { useState } from 'react'
import EmployeeDetail from '../EmployeeDetail/EmployeeDetail'
import { ReactComponent as ArrowIcon } from '../../../shared/assets/icons/arrow.svg'
import { ReactComponent as EmailIcon } from '../../../shared/assets/icons/employees/email.svg'
import { ReactComponent as PhoneIcon } from '../../../shared/assets/icons/employees/phone.svg'

export interface IEmployee {
  employee: {
    id: number
    created_at: Date
    fullName: string
    photo: any
    email: string
    role: string
    gender: string
    birthday: string
    address: string
    phoneNumber1: string
    phoneNumber2: string
    job: string
    department: string
    status: any
    hiringDate: string
  }
}

function EmployeeRow({ employee }: IEmployee) {
  const [showEmployeeDetails, setShowEmployeeDetails] = useState([0])

  const { id, photo, fullName, job, department, status, email, phoneNumber1 } = employee

  function handleShowDetails() {
    if (showEmployeeDetails.includes(id))
      setShowEmployeeDetails(showEmployeeDetails.filter((el) => el !== id))
    else setShowEmployeeDetails([...showEmployeeDetails, id])
  }

  let employeeStatus
  if (status.id === 1) employeeStatus = <div className="employee-status full-time">Full Time</div>
  if (status.id === 2) employeeStatus = <div className="employee-status part-time">Part Time</div>

  return (
    <>
      <div className="employees-table-el">{id}</div>
      <div className="employees-table-el">
        {photo ? (
          <img className="avatar" src={photo.path} />
        ) : (
          <div className="avatar avatar-letter">{fullName.slice(0, 1)}</div>
        )}
        <div className="flex-col">
          <span className="employee-fullname">{fullName}</span>
          <span className="employee-job">{job}</span>
        </div>
      </div>
      <div className="employees-table-el">{department} Department</div>
      <div className="employees-table-el">{employeeStatus}</div>
      <div className="employees-table-el flex-col">
        <span className="flex">
          <EmailIcon /> {email}
        </span>{' '}
        <span className="flex">
          <PhoneIcon /> {phoneNumber1}
        </span>
      </div>
      <div className="employees-table-el">
        <div className="employee-dropdown" onClick={handleShowDetails}>
          <ArrowIcon />
        </div>
      </div>

      {showEmployeeDetails.includes(id) && (
        <EmployeeDetail employee={employee} handleShowDetails={handleShowDetails} />
      )}
    </>
  )
}

export default EmployeeRow
