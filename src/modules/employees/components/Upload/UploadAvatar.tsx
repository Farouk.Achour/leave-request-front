import { Dispatch, SetStateAction, useState } from 'react'
import { Upload, UploadFile, UploadProps } from 'antd'

import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import axiosInstance from '@src/modules/shared/utils/axios'

interface IUploadAvatarProps {
  avatar: any
  setAvatar: Dispatch<SetStateAction<any>>
  supabaseBucket?: string
}

function UploadAvatar({ avatar, setAvatar }: IUploadAvatarProps) {
  const [loading, setLoading] = useState(false)
  const [fileList, setFileList] = useState<UploadFile[]>([])

  const customRequest: UploadProps['customRequest'] = async ({ file, onSuccess, onError }) => {
    try {
      setLoading(true)

      const formData = new FormData()
      formData.append('file', file)

      const { data } = await axiosInstance.post('files/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })

      setAvatar({ path: data.file.path, id: data.file.id })

      onSuccess?.('File uploaded successfully!')
      setLoading(false)
    } catch (error) {
      onError?.({ message: 'File upload failed.', name: 'Upload Error' })
      setLoading(false)
    }
  }

  const onChange: UploadProps['onChange'] = ({ fileList }) => {
    setFileList(fileList)
  }

  const onRemove: UploadProps['onRemove'] = (file) => {
    const newFileList = fileList.filter((item) => item.uid !== file.uid)
    setFileList(newFileList)
  }

  const uploadProps: UploadProps = {
    customRequest,
    fileList: [],
    onChange,
    onRemove,
  }

  const uploadButton = (
    <button
      style={{
        border: 0,
        background: '#DBDBDB',
        width: '94px',
        height: '94px',
        color: '#8e8e8e',
        borderRadius: '100%',
        cursor: 'pointer',
      }}
      type="button"
    >
      {loading ? <LoadingOutlined /> : <PlusOutlined style={{ transform: 'scale(1.5)' }} />}
    </button>
  )

  return (
    <Upload {...uploadProps}>
      {loading ? (
        uploadButton
      ) : avatar.path ? (
        <img
          src={avatar.path}
          alt="avatar"
          style={{ width: '94px', height: '94px', borderRadius: '100%', cursor: 'pointer' }}
        />
      ) : (
        uploadButton
      )}
    </Upload>
  )
}

export default UploadAvatar
