/* eslint-disable @typescript-eslint/no-explicit-any */
import MainLayout from '@src/modules/shared/layout/MainLayout/MainLayout'
import AuthGuard from '@src/modules/shared/guards/AuthGuard'
import { RouteProps } from 'react-router-dom'
import { Fragment, lazy } from 'react'

type RouteConfig = {
  exact: boolean | null
  path: string
  component: React.ComponentType<any>
  guard?: React.ComponentType<any> | typeof Fragment | any
  layout?: React.ComponentType<any> | typeof Fragment
} & RouteProps

const routes: RouteConfig[] = [
  // AuthGuard Routes
  {
    exact: true,
    guard: AuthGuard,
    path: '/employees',
    component: lazy(() => import('../features/EmployeesLayout')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/employees/add',
    component: lazy(() => import('../features/AddEmployee/AddEmployee')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/employees/info',
    component: lazy(() => import('../features/EmployeeDetails/EmployeeDetails')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/employees/edit',
    component: lazy(() => import('../features/UpdateEmployee/UpdateEmployee')),
    layout: MainLayout,
  },
]

export default routes
