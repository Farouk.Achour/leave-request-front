import EmployeeRow from '../../components/EmployeeRow/EmployeeRow'
import EmployeesTableNav from '../../components/EmplyeesTableNav/EmployeesTableNav'
import { useEmployees } from '../../hooks/useEmployees'

interface IEmployeesTableProps {
  searchedEmployee: string
}

function EmployeesTable({ searchedEmployee }: IEmployeesTableProps) {
  const { employees } = useEmployees()

  return (
    <div className="employees-table">
      <EmployeesTableNav />
      {employees
        ?.filter((el: any) => el.fullName.toLowerCase().includes(searchedEmployee.toLowerCase()))
        .map((el: any) => <EmployeeRow employee={el} key={el.id} />)}
    </div>
  )
}

export default EmployeesTable
