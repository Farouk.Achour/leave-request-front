import { useState } from 'react'
import EmployeesHeader from './EmployeesHeader/EmployeesHeader'
import EmployeesTable from './EmployeesTable/EmployeesTable'

const EmployeesLayout = () => {
  const [searchedEmployee, setSearchedEmployee] = useState('')

  return (
    <div className="employees-layout">
      <EmployeesHeader
        searchedEmployee={searchedEmployee}
        setSearchedEmployee={setSearchedEmployee}
      />
      <EmployeesTable searchedEmployee={searchedEmployee} />
    </div>
  )
}

export default EmployeesLayout
